const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const db = require('../utils/conn');

/**
 * 评价Schema
 */
const rateSchema = new Schema({
    "comment": String,
    "rating": {
        "count": Number,
        "max": Number,
        "star_count": Number,
        "value": Number
    },
    "vote_count": Number,
    "create_time": Date,
    "user": {
        "id": String,
        "uid": String
    },
    "id": {
        type: String,
        unique: true
    }
});

const rateModel = db.model('Rate', rateSchema);

module.exports = rateModel;