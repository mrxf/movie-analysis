const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const db = require('../utils/conn');

/**
 * 抓取进度Schema
 */
const InfoSchema = new Schema({
    rate: {
        nextStartPage: {
            type: Number,
            default: 1
        },
        count: {
            type: Number,
            default: 0
        },
        total: {
            type: Number,
            default: 0
        },
        totalPage: {
            type: Number,
            default: 0
        },
        limit: {
            type: Number,
            default: 50
        },
        interval: {
            type: Number,
            default: 3000
        }
    }
});

const InfoModel = db.model('Info', InfoSchema);

module.exports = InfoModel;