const request = require('request');
const CrawlerValue = require('../variables/crawler');
const RateModel = require('../models/rate.model');
const InfoDao = require('./infos.dao');
const _ = require('lodash');

class RateCrawler {
    constructor() {
        this.crawlerInfo = {}
        this.crawlerClock = null;
        this.isInitTotal = false;
    }

    /**
     * 初始化爬虫信息
     */
    async init() {
        this.infoDao = new InfoDao();
        const infos = await this.infoDao.findInfos();
        this.dbInfos = infos;  // 将数据库里的Info信息存为变量
        this.crawlerInfo = Object.assign({}, infos["rate"]);

        /**
         * 如果总数大于0，那么代表已经初始化过总数
         */
        if (infos["rate"].total > 0) {
            this.isInitTotal = true;
        }
    }

    /**
     * 开始执行
     */
    start() {
        this.init().then(_ => {
            this.crawlerClock = setInterval(() => {
                console.log(`开始抓取第${this.crawlerInfo.nextStartPage}页`);
                /**
                * 先判断是否已经超出抓取页
                * 1. 如果存在总数信息，并且nextStartPage超过了总页数，那么结束
                * 2. 如果不存在总数信息，那么继续抓取，并且初始化总数信息
                */
                if(this.isInitTotal && (this.crawlerInfo.nextStartPage > this.crawlerInfo.totalPage)) {
                    console.log("抓取结束");
                    clearInterval(this.crawlerClock);
                    return true;
                }

                /** 不断抓取 */
                this.getDoubanData();
                this.crawlerInfo.nextStartPage++;
            }, this.crawlerInfo.interval)
        });
    }

    /**
     * 获取豆瓣数据
     */
    getDoubanData() {
        const start = this.crawlerInfo.nextStartPage * this.crawlerInfo.limit;
        request({
            uri: CrawlerValue.RATE_URL,
            qs: {
                count: this.crawlerInfo.limit,
                start: start
            },
            headers: {
                "User-Agent": CrawlerValue.USER_AGENT
            }
        }, (err, res, body) => {
            const rateInfo = JSON.parse(body);
            if(rateInfo.hasOwnProperty("interests")) {
                this.saveData(rateInfo["interests"]);
                this.saveCrawledPage();
            }
            
            // 如果没有初始化总数信息，初始化总数/总页数信息
            if(this.isInitTotal === false) {
                this.initTotal(rateInfo);
            }
        })
    }

    /**
     * 分别保存评论数据里的信息
     * @param {array} interests 评论信息数组
     */
    saveData(interests) {
        interests.forEach(v => {
            this.saveInterest(v);
        });
    }

    /**
     * 保存评分评论信息
     * @param {*} v 评论数据
     */
    saveInterest(v) {
        const rateModel = new RateModel({
            "comment": v["comment"],
            "rating": {
                "count": (v.rating || {}).count,
                "max": (v.rating || {}).max,
                "star_count": (v.rating || {}).star_count,
                "value": (v.rating || {}).value
            },
            "vote_count": v["vote_count"],
            "create_time": new Date(v["create_time"]),
            "user": {
                "id": v["user"]["id"],
                "uid": v["user"]["uid"]
            },
            "id": v["id"]
        });
        let query = rateModel.save();
        query.then(res => {
            // 保存数据成功
            // console.log("保存成功")
        }, err => {
            if(err.code === 11000) {
                // 重复的id
                console.log("重复id");
            }
        })
    }

    /**
     * 初始化总条数和总页数
     */
    async initTotal(rateInfo) {
        const infos = _.cloneDeep(this.dbInfos);
        this.isInitTotal = true;  // 默认已经初始化成功
        this.crawlerInfo.total = rateInfo.total; // 总条数
        this.crawlerInfo.totalPage = Math.ceil(rateInfo.total / infos["rate"].limit);  // 总页数

        infos["rate"] = Object.assign({}, this.crawlerInfo);
        const updateQuery =  this.infoDao.updateInfos(infos);

        updateQuery.then(res => {
            console.log("初始化总数信息结束");
        }, err => {
            this.isInitTotal = false;
        })
    }

    /**
     * 保存抓取的页数
     */
    saveCrawledPage() {
        const infos = _.cloneDeep(this.dbInfos);
        infos["rate"] = Object.assign({}, this.crawlerInfo);
        infos["rate"].nextStartPage = this.crawlerInfo.nextStartPage + 1;
        const updateQuery =  this.infoDao.updateInfos(infos);
        
        updateQuery.then(res => {
            console.log("保存页数结束");
        }, err => {
            // 错误
        })
    }
}

module.exports = RateCrawler;
