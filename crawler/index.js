const RateCrawler = require('./rate-crawler');
const InfosDao = require('./infos.dao');

const infosDao = new InfosDao();
/**
 * 保证初始化信息完成
 */
infosDao.init().then(res => {
    const rateCrawler = new RateCrawler();
    rateCrawler.start();
})