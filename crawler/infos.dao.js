const db = require('../utils/conn');
const InfoModel = require('../models/infos.model');

class InfosDao {
    constructor() {}

    /**
     * 初始化爬虫信息
     */
    async init() {
        const query = InfoModel.findOne({});
        const infos = await query.exec();
        let result = null;
        if(!infos || infos.length < 1) {
            try {
                const initInfos = new InfoModel();
                result = initInfos.save();
                console.log("初始化配置信息！");
            }catch(e) {
                console.error(e);
            }
        }else {
            result = new Promise(reslove => {
                reslove("已存在配置信息");
            })
            console.log("已存在配置信息");
        }
        return result;
    }

    /**
     * 获取爬虫抓取信息
     */
    findInfos() {
        return InfoModel.findOne({});
    }

    /**
     * 更新爬虫信息
     */
    updateInfos(oInfo) {
        return InfoModel.update({_id: oInfo._id}, oInfo);
    }

}


module.exports = InfosDao;