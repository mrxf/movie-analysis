const fs = require('fs');
const md5 = require('md5');
const moment = require('moment');

/**
 * 错误信息处理工具
 * 1. 将错误信息保存到根目录的error.log文件中
 * 2. 输出简略Error信息，以及错误对应的MD5摘要
 * @param { Error } error 错误信息
 */
function errorHandler(error){

    /**
     * 将错误内容信息变为可以存储的字符串类型
     */
    let stringError = error.stack;
    
    /** 获取错误信息的hash值 */
    let errorHash = md5(stringError);

    /** 获取当前时间 */
    const currentTime = new Date();
    const formatTime = moment(currentTime).format('YYYY-MM-DD HH:mm:ss');

    /** 定义错误字符串模板 */
    const errorInfo = `
            =============${errorHash}============
            =============${formatTime}===========\n${stringError}
    `;

    /**
     * 将错误写入log文件中
     */
    fs.appendFile('../logs/error.log', errorInfo, (err) => {
        if (err) throw err;
        console.log(`===出现错误:=== ${error} \n===错误已保存=== ${errorHash}`);
    });
}

module.exports = errorHandler;