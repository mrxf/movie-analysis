const bluebird = require('bluebird');
const mongoose = require('mongoose');
const errorHandler = require('./errorHandler');
const DATABASE = require('../variables/database')

mongoose.Promise = bluebird;
/**
 * 连接选项
 */
const url = `mongodb://${DATABASE.USER_NAME}:${DATABASE.PASS_WORD}@${DATABASE.URI}:${DATABASE.PORT}/${DATABASE.DATABASE_NAME}`;
let db;
try{
    db = mongoose.createConnection(url);
}catch(error){
    errorHandler(error);
}


module.exports = db;